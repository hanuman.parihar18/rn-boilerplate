import * as CONST from '../utils/constants';

export function startUp() {
  return {
    type: CONST.START_UP,
  };
}
