import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Dimensions, ScrollView
} from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import I18n from '../../i18n/index';
import NavigationService from '../../services/navigationService';
import * as CONST from '../../utils/constants';
import * as brandActions from '../../actions/brandActions';

import styles from './AboutUsStyles';

const { height, width } = Dimensions.get('window');

class AboutUsTabComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      aboutUsText: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using making it look like readable English.',
    };
  }

  componentDidMount() {
  }

  onPressCrossIcon() {
    NavigationService.goBack();
  }

  renderBrandsHeader() {
    return (
      <View style={styles.brandsHeader}>
        <Text style={styles.brandsText}>About</Text>
        <TouchableOpacity onPress={() => this.onPressCrossIcon()} style={styles.crossIcon}>
          <Icons name="ios-close" size={40} color={CONST.BORDER_COLOR_GREY_LIGHT} />
        </TouchableOpacity>
      </View>
    );
  }

  renderAboutUsView() {
    return (
      <View style={styles.aboutUs}>
        <Text style={styles.aboutUsText}>{this.state.aboutUsText}</Text>
      </View>
    );
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {this.renderBrandsHeader()}
        {this.renderAboutUsView()}
      </ScrollView>

    );
  }
}

const mapStateToProps = (state) => ({
  filters: state.BrandReducer.filters,
  allBrands: state.BrandReducer.allBrands,
  allCategories: state.BrandReducer.allCategories,
});

const mapDispatchToProps = (dispatch) => ({
  setAllFilters: () => {
    return dispatch(brandActions.setAllFilters());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutUsTabComponent);
