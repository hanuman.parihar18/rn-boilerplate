import React from 'react';
import {
  View, TouchableOpacity, Image
} from 'react-native';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
import styles from './FBAuthStyles';
import * as CONST from '../../utils/constants';

const Analytics = firebase.analytics();
let fbToken = null;

export default function FBAuthComponent({ props }) {
  const renderFB = () => {
    LoginManager.logInWithPermissions(['email']).then(
      (result) => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
          renderFbData();
        }
      },
      (error) => {
        console.log(`Login fail with error: ${error}`);
      }
    );
  };

  const renderFbData = () => {
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        fbToken = {
          accessToken: data.accessToken
        };
        console.log(`result from facebook login : ${JSON.stringify(data)}`);
      }
    );

    const infoRequest = new GraphRequest(
      '/me?fields=email,name,picture.type(large)',
      null,
      _responseInfoCallback,
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  };

  const _responseInfoCallback = (error, result) => {
    if (error) {
      console.log(`Error fetching data: ${error.toString()}`);
    } else {
      const user = {
        name: result.name,
        email: result.email,
        photo: result.picture.data.url,
        accessToken: fbToken,
        idToken: result.id
      };
      console.log('FB USER INFO', user);
    }
  };

  return (
    <TouchableOpacity onPress={() => renderFB()} style={styles.fbSocialIcon}>
      <Image source={CONST.FB_ICON} />
    </TouchableOpacity>
  );
}
