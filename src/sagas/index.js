import { takeLatest, all } from 'redux-saga/effects';
// import { startup } from './startupSaga';
import { userLogin, userLogout, userSignup, updateUserProfile } from './userLoginSaga';
import { getAllBrands, getAllCategories } from './brandSaga';
import { getAllProducts, getAllFavouriteProducts, markProductAsFavourite, markProductAsUnFavourite, getProductDetails, submitProductFeedback, getProductFeedbacks, buyProduct, getAllCoupons, searchProduct } from './productsSaga';
import { getAllOrders } from './ordersSaga';
import * as CONST from '../utils/constants';
import * as PRODUCT_CONST from '../actions/productActions';
import * as ORDER_CONST from '../actions/orderActions';

export default function* root() {
  yield all([
    // takeLatest(CONST.START_UP, startup),
    takeLatest(CONST.USER_LOGIN, userLogin),
    takeLatest(CONST.USER_LOGOUT, userLogout),
    takeLatest(CONST.USER_SIGNUP, userSignup),
    takeLatest(CONST.GET_ALL_BRANDS, getAllBrands),
    takeLatest(CONST.GET_ALL_CATEGORIES, getAllCategories),
    takeLatest(CONST.GET_ALL_PRODUCTS, getAllProducts),
    takeLatest(CONST.UPDATE_USER_PROFILE, updateUserProfile),
    takeLatest(CONST.SUBMIT_PRODUCT_FEEDBACK, submitProductFeedback),
    takeLatest(PRODUCT_CONST.MARK_FAVOURITE_PRODUCTS, markProductAsFavourite),
    takeLatest(PRODUCT_CONST.UNMARK_FAVOURITE_PRODUCTS, markProductAsUnFavourite),
    takeLatest(PRODUCT_CONST.GET_ALL_FAVOURITE_PRODUCTS, getAllFavouriteProducts),
    takeLatest(PRODUCT_CONST.GET_PRODUCT_DETAILS, getProductDetails),
    takeLatest(PRODUCT_CONST.GET_PRODUCT_FEEDBACKS, getProductFeedbacks),
    takeLatest(ORDER_CONST.GET_ALL_ORDERS, getAllOrders),
    takeLatest(CONST.BUY_PRODUCTS, buyProduct),
    takeLatest(CONST.GET_ALL_COUPONS, getAllCoupons),
    takeLatest(CONST.SEARCH_PRODUCTS, searchProduct),
  ]);
}
