import * as CONST from '../utils/constants';


const initialState = {
  user: null,
  message: '',
};

export type State = {
  user: any,
  message: any,
}


// This reducer stores the status of email verification.
export default function (state:State = initialState, action:Action): State {
  switch (action.type) {
    case CONST.USER_LOGIN:
      return {
        ...state,
        user: null,
        message: null,
      };
    case CONST.USER_LOGIN_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        message: 'successfully logged in',
      };
    case CONST.USER_LOGIN_FAILED:
      return {
        ...state,
        user: null,
        message: 'error in login process',
      };

    case CONST.USER_SIGNUP:
      return {
        ...state,
        user: null,
        message: null,
      };
    case CONST.USER_SIGNUP_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        message: 'successfully signed up',
      };
    case CONST.USER_SIGNUP_FAILED:
      return {
        ...state,
        user: null,
        message: 'error in signup process',
      };

    case CONST.USER_LOGOUT:
      return {
        ...state,
        message: '',
      };
    case CONST.USER_LOGOUT_SUCCESS:
      return {
        user: null,
        message: CONST.USER_LOGGED_OUT_SUCCESSFULLY,
      };
    case CONST.USER_LOGOUT_FAILURE:
      return {
        message: CONST.ERROR_IN_LOGOUT,
      };
    case CONST.UPDATE_USER_PROFILE_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
      };
    case CONST.UPDATE_USER:
      return {
        ...state,
        user: action._user
      };
    default:
      return state;
  }
}
