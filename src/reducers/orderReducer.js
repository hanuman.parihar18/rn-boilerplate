import * as ORDER_CONST from '../actions/orderActions';
import * as PRODUCT_CONST from '../actions/productActions';

const initialState = {
  userOrders: null
};

// This reducer stores the status of email verification.
export default function (state = initialState, action:Action): State {
  switch (action.type) {
    case ORDER_CONST.GET_ALL_ORDERS:
      return {
        ...state,
        userOrders: null
      };
    case ORDER_CONST.GET_ALL_ORDERS_SUCCESS:
      return {
        ...state,
        userOrders: action.payload,
      };
    case ORDER_CONST.GET_ALL_ORDERS_FAILURE:
      return {
        ...state,
      };
    case PRODUCT_CONST.CHANGED_FAVOURITE_STATUS:
      return {
        ...state,
        userOrders: action.data
      }
    default:
      return { ...state };
  }
}
